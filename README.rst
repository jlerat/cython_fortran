
==============
cython_fortran
==============

Example on binding fortran and C code to python via cython


What is cython_fortran?
=======================

-  Provides examples of c and fortran code to be called from python
-  All compilation is handled by Makefile that are independent from a python packaging system.
-  The binding of c and fortran code is performed by Cython.

Installation
============

The code can be downloaded by cloning the repository from `Bitbucket <https://bitbucket.org/jlerat/cython_fortran>`_
.. code-block:: bash 

    git clone https://bitbucket.org/jlerat/cython_fortran.git

Content
=======

The repository contains several examples of c/fortran bindings with increasing complexity:

- Example 1 (ex01) demonstrate how to bind c and fortran code performing the cumulative sum of a vector of doubles. This is a basic example of timeseries modelling. The example contains the following files:
    +  Makefile : Compiling script
    +  c_ex01.c : C code performing the cumulative sum
    +  c_ex01.h : Header of the C code 
    +  f_ex01.f90 : Fortan code performing the cumulative sum
    +  f_ex01.h : Header of the Fortran code 
    +  f_ex01_wrap.f90 : Fortran wrapper function to bind C to Fortan. This function is required to map the Fortran variables to C variables that are used within Cython.
    + ex01_kernel.pyx : Cython module calling both C and Fortran codes.
    + ex01.py : Python function calling the Cython module. The type of binding (c or fortran) can be passed as an argument to the function
    + test_ex01.py : Test suite for the python function ex01.
    
    To run the example:
    .. code-block:: bash

        cd ex01 
        
        make
        
        nosetests

    Example of use:
    .. code-block:: python
      
        from ex01 import ex01
        
        import numpy as np

        nval = 100
        
        a = np.random.uniform(0, 10, nval)

        # Cumulative sum with fortran binding
        
        bf = ex01(a, 'fortran')

        # Cumulative sum with c binding
        
        bc = ex01(a, 'c')

Dependencies
============

The examples are designed assuming that the following packages are availabe

- ``Python 2.7`` with the following packages installed (packages can be easily installed via `Anaconda <https://continuum.io/downloads>`_)

    + Numpy
    + nose
    
- ``gcc`` C compiler (on ubuntu, it can be installed by running ``sudo apt-get install build-essential``)
- ``gfortran`` Fortran compiler (on ubuntu, it can be installed by running ``sudo apt-get install gfortran``)

