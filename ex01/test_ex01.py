import os, re, math, json
import unittest

import numpy as np

from ex01 import ex01

np.seterr(all='print')

class Ex01TestCases(unittest.TestCase):

    def setUp(self):
        print('\t=> Ex01TestCase')

    def test_ex01_fortan_1(self):
        nval = 10
        a = np.random.uniform(0, 10, nval)
        b = np.cumsum(a)
        c = ex01(a)
        ck = np.allclose(b, c)
        self.assertTrue(ck)

    def test_ex01_fortran_2(self):
        nval = 10
        a = -1. * np.ones(nval)
        try:
            c = ex01(a)
        except ValueError, e:
            message = str(e)
        self.assertTrue(message.startswith('binded fortran function returns 1'))

    def test_ex01_c_1(self):
        nval = 10
        a = np.random.uniform(0, 10, nval)
        b = np.cumsum(a)
        c = ex01(a, 'c')
        ck = np.allclose(b, c)
        self.assertTrue(ck)

    def test_ex01_c_2(self):
        nval = 10
        a = -1. * np.ones(nval)
        try:
            c = ex01(a, 'c')
        except ValueError, e:
            message = str(e)
        self.assertTrue(message.startswith('binded c function returns 1'))

