#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

int c_ex01_run(int ninputs, double * inputs, double * outputs)
{
    int i;
    double val, sumval=0.;

    for(i=0; i<ninputs; i++){
        val = inputs[i];
        sumval += val;
        if(val>=0)
            outputs[i] = sumval;
        else
            return 1;
    }

    return 0;
}


