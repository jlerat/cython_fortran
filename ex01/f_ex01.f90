
module f_ex01

    implicit none

    integer, parameter :: dp=kind(0.d0) ! Double precision
    
    contains

    subroutine f_ex01_run(ierr, nrows, inputs, outputs)

        integer, intent(out) :: ierr(:)
        integer, intent(in), value :: nrows
        real(dp), dimension(nrows), intent(in) :: inputs
        real(dp), dimension(nrows), intent(out) :: outputs
        real(dp) :: sumval, val
        integer :: i

        sumval = 0.
        ierr(1) = 0

        ! Compute cumulative sum
        do i=1,nrows
            
            ! Check inputs
            val = inputs(i)
            if(val .lt. 0) then 
                ierr(1) = 1
                return
            endif

            ! Run model
            sumval = sumval + val
            outputs(i) = sumval
            write(*,*) "csumum(", val, ") = ", outputs(i)

        enddo

    end subroutine f_ex01_run

end module f_ex01

