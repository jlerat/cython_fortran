import numpy as np
cimport numpy as np

np.import_array()

# -- HEADERS --

cdef extern from 'f_ex01_wrap.h':
    void f_ex01_run_wrap(int *ierr, int ninputs, double * inputs,
                    double * outputs)

cdef extern from 'c_ex01.h':
    int c_ex01_run(int ninputs, double * inputs, double * outputs)


def __cinit__(self):
    pass


def f_ex01(np.ndarray[double, ndim=1, mode='c'] inputs not None,
        np.ndarray[double, ndim=1, mode='c'] outputs not None):

    cdef int ierr[1];
    ierr[0] = 0

    # check dimensions
    if inputs.shape[0] != outputs.shape[0]:
        raise ValueError('inputs.shape[0] != outputs.shape[0]')

    # Run model
    f_ex01_run_wrap(ierr,
            inputs.shape[0],
            <double*> np.PyArray_DATA(inputs),
            <double*> np.PyArray_DATA(outputs))

    return ierr[0]


def c_ex01(np.ndarray[double, ndim=1, mode='c'] inputs not None,
        np.ndarray[double, ndim=1, mode='c'] outputs not None):

    cdef int ierr;

    # check dimensions
    if inputs.shape[0] != outputs.shape[0]:
        raise ValueError('inputs.shape[0] != outputs.shape[0]')

    # Run model
    ierr = c_ex01_run(inputs.shape[0],
            <double*> np.PyArray_DATA(inputs),
            <double*> np.PyArray_DATA(outputs))

    return ierr




