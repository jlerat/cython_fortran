
module f_ex01_wrap

    use iso_c_binding, only: c_double, c_int
    use f_ex01, only: f_ex01_run

    implicit none

    contains

    subroutine f_ex01_run_wrap(c_ierr, c_ninputs, c_inputs, c_outputs) bind(c)

        integer(c_int), intent(in), value :: c_ninputs
        integer(c_int), dimension(1), intent(out) :: c_ierr
        real(c_double), dimension(c_ninputs), intent(in) :: c_inputs
        real(c_double), dimension(c_ninputs), intent(out) :: c_outputs

        call f_ex01_run(c_ierr, c_ninputs, c_inputs, c_outputs)

    end subroutine f_ex01_run_wrap

end module f_ex01_wrap

