
import numpy as np
from ex01_kernel import f_ex01, c_ex01

def ex01(inputs, binding='fortran'):

    inputs = np.atleast_1d(inputs).astype(np.float64)

    outputs = np.zeros_like(inputs).astype(np.float64)

    if binding == 'fortran':
        ierr = f_ex01(inputs, outputs)
    else:
        ierr = c_ex01(inputs, outputs)

    if ierr > 0:
        raise ValueError('binded {0} function returns {1}'.format(binding, ierr))

    return outputs
